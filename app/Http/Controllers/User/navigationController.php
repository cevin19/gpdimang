<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\jadwal_pelayanan;
use App\Model\firmanMinggu;
use App\Model\jadwal_ibadah;
use App\Model\galeri;
use Carbon\Carbon;

class navigationController extends Controller
{
    public function jadwalIbadah(){
        $weekMap = [
            0 => 'Minggu',
            1 => 'Senin',
            2 => 'Selasa',
            3 => 'Rabu',
            4 => 'Kamis',
            5 => 'Jumat',
            6 => 'Sabtu',
        ];
        // $dayOfTheWeek = Carbon::now()->dayOfWeek;
        // return $weekday = $weekMap[$dayOfTheWeek];  
        $datas= jadwal_ibadah::all()->sortBy('waktu');
        return view('user.jadwalIbadah',compact('datas','weekMap'));
    }
    public function jadwalPelayanan(){
        $pagi = jadwal_pelayanan::where('waktu_ibadah',1)->get()->first();    
        $siang = jadwal_pelayanan::where('waktu_ibadah',2)->get()->first();    
        $sore = jadwal_pelayanan::where('waktu_ibadah',3)->get()->first();    
        
        return view('user.jadwalPelayanan',compact('pagi','siang','sore'));
    }
    public function beranda(){
        $data= firmanMinggu::where('id',1)->get()->first();

        return view('user.home',compact('data'));
    }

    public function tentangGereja(){
        return view('user.about');
    }

    public function galeri(){
        $datas= galeri::all()->sortBy('created_at');
        return view('user.galeri',compact('datas'));
    }
}
