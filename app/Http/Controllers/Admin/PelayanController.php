<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\jenis_pelayanan;
use App\Model\pelayan;

class PelayanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pelayans = pelayan::all();
        return view('admin.pelayan.index',compact('pelayans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $jenis_pelayanan = jenis_pelayanan::all();
        return view('admin.pelayan.create',compact('jenis_pelayanan'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'jenis_pelayanan'=>'required'
        ]);
        
        $pelayan = new pelayan;
        $pelayan->name = $request->name;
        $pelayan->save();
        $pelayan->pelayanans()->sync($request->jenis_pelayanan);
        return redirect(route('pelayan.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pelayan = pelayan::with('pelayanans')->where('id',$id)->first();
        $jenis_pelayanan = jenis_pelayanan::all();
        return view('admin.pelayan.edit',compact('jenis_pelayanan','pelayan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'=>'required',
            'jenis_pelayanan'=>'required'
        ]);
        
        $pelayan = pelayan::find($id);
        $pelayan->name = $request->name;
        $pelayan->update();
        $pelayan->pelayanans()->sync($request->jenis_pelayanan);
        return redirect(route('pelayan.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pelayan = pelayan::find($id);
        $pelayan->delete();
        return redirect(route('pelayan.index'));
    }
}
