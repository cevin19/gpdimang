<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\galeri;
class galeriController extends Controller
{
    public function index()
    {

        $galeris = galeri::all();

        return view('admin.galeri.index',compact('galeris'));
    }
    public function create()
    {
        return view('admin.galeri.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'nama'=>'required',
            'link'=>'required',
            'foto_depan'=>'required'
        ]);
        
        $galeri = new galeri;
        $galeri->nama = $request->nama;
        $galeri->link = $request->link;
        $galeri->foto_depan = $request->foto_depan;
        $galeri->save();
        return redirect(route('galeri.index'));
    }

    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
