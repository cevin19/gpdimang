<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\jenis_pelayanan;
use App\Model\pelayan;
use App\Model\jadwal_pelayanan;

class jadwalPelayananController extends Controller
{
    public function pagi(){
        $data = jadwal_pelayanan::where('waktu_ibadah',1)->get()->first();    
        // data untuk datalist pelayan
        $musiks = jenis_pelayanan::with('pelayans')->where('nama','Musik')->get();
        $singers = jenis_pelayanan::with('pelayans')->where('nama','Singer')->get();
        $wls = jenis_pelayanan::with('pelayans')->where('nama','Whorship Leader' )->get();
        $multimedias = jenis_pelayanan::with('pelayans')->where('nama','Multimedia' )->get();
        $kolektans = jenis_pelayanan::with('pelayans')->where('nama','Kolektan' )->get();
        $penerimas = jenis_pelayanan::with('pelayans')->where('nama','Penerima Tamu' )->get();
        
        return view('admin.jadwalPelayanan.pagi',compact('musiks','singers','data','wls','multimedias','kolektans','penerimas'));
    }

    public function siang(){
        $data = jadwal_pelayanan::where('waktu_ibadah',2)->get()->first();    
        // data untuk datalist pelayan
        $musiks = jenis_pelayanan::with('pelayans')->where('nama','Musik')->get();
        $singers = jenis_pelayanan::with('pelayans')->where('nama','Singer')->get();
        $wls = jenis_pelayanan::with('pelayans')->where('nama','Whorship Leader' )->get();
        $multimedias = jenis_pelayanan::with('pelayans')->where('nama','Multimedia' )->get();
        $kolektans = jenis_pelayanan::with('pelayans')->where('nama','Kolektan' )->get();
        $penerimas = jenis_pelayanan::with('pelayans')->where('nama','Penerima Tamu' )->get();
        
        return view('admin.jadwalPelayanan.siang',compact('musiks','singers','data','wls','multimedias','kolektans','penerimas'));
    }

    public function sore(){
        $data = jadwal_pelayanan::where('waktu_ibadah',3)->get()->first();    
        // data untuk datalist pelayan
        $musiks = jenis_pelayanan::with('pelayans')->where('nama','Musik')->get();
        $singers = jenis_pelayanan::with('pelayans')->where('nama','Singer')->get();
        $wls = jenis_pelayanan::with('pelayans')->where('nama','Whorship Leader' )->get();
        $multimedias = jenis_pelayanan::with('pelayans')->where('nama','Multimedia' )->get();
        $kolektans = jenis_pelayanan::with('pelayans')->where('nama','Kolektan' )->get();
        $penerimas = jenis_pelayanan::with('pelayans')->where('nama','Penerima Tamu' )->get();
        
        return view('admin.jadwalPelayanan.sore',compact('musiks','singers','data','wls','multimedias','kolektans','penerimas'));
    }

    public function updatePagi(Request $request, $id)
    {
        
        $this->validate($request,[
            'whorship_leader'  =>  'required',
            'pemain_musik_1'  =>  'required',
            'multimedia'  =>  'required',
            'singer_1'  =>  'required',
            'kolektan_1'  =>  'required',
            'penerima_tamu_1'  =>  'required',
        ]);

        $data =  jadwal_pelayanan::find($id);
        $data->whorship_leader = $request->whorship_leader;
        $data->pemain_musik_1 = $request->pemain_musik_1;
        $data->pemain_musik_2 = $request->pemain_musik_2;
        $data->pemain_musik_3 = $request->pemain_musik_3;
        $data->pemain_musik_4 = $request->pemain_musik_4;
        $data->multimedia = $request->multimedia;
        $data->singer_1 = $request->singer_1;
        $data->singer_2 = $request->singer_2;
        $data->singer_3 = $request->singer_3;
        $data->kolektan_1 = $request->kolektan_1;
        $data->kolektan_2 = $request->kolektan_2;
        $data->penerima_tamu_1 = $request->penerima_tamu_1;
        $data->penerima_tamu_2 = $request->penerima_tamu_2;    
        $data->update();
        
        return redirect(route('jPelayanan.pagi'));

    }
    public function updateSiang(Request $request, $id)
    {
        
        $this->validate($request,[
            'whorship_leader'  =>  'required',
            'pemain_musik_1'  =>  'required',
            'multimedia'  =>  'required',
            'singer_1'  =>  'required',
            'kolektan_1'  =>  'required',
            'penerima_tamu_1'  =>  'required',
        ]);

        $data =  jadwal_pelayanan::find($id);
        $data->whorship_leader = $request->whorship_leader;
        $data->pemain_musik_1 = $request->pemain_musik_1;
        $data->pemain_musik_2 = $request->pemain_musik_2;
        $data->pemain_musik_3 = $request->pemain_musik_3;
        $data->pemain_musik_4 = $request->pemain_musik_4;
        $data->multimedia = $request->multimedia;
        $data->singer_1 = $request->singer_1;
        $data->singer_2 = $request->singer_2;
        $data->singer_3 = $request->singer_3;
        $data->kolektan_1 = $request->kolektan_1;
        $data->kolektan_2 = $request->kolektan_2;
        $data->penerima_tamu_1 = $request->penerima_tamu_1;
        $data->penerima_tamu_2 = $request->penerima_tamu_2;    
        $data->update();
                
        return redirect(route('jPelayanan.siang'));
        

    }
    public function updateSore(Request $request, $id)
    {
        
        $this->validate($request,[
            'whorship_leader'  =>  'required',
            'pemain_musik_1'  =>  'required',
            'multimedia'  =>  'required',
            'singer_1'  =>  'required',
            'kolektan_1'  =>  'required',
            'penerima_tamu_1'  =>  'required',
        ]);

        $data =  jadwal_pelayanan::find($id);
        $data->whorship_leader = $request->whorship_leader;
        $data->pemain_musik_1 = $request->pemain_musik_1;
        $data->pemain_musik_2 = $request->pemain_musik_2;
        $data->pemain_musik_3 = $request->pemain_musik_3;
        $data->pemain_musik_4 = $request->pemain_musik_4;
        $data->multimedia = $request->multimedia;
        $data->singer_1 = $request->singer_1;
        $data->singer_2 = $request->singer_2;
        $data->singer_3 = $request->singer_3;
        $data->kolektan_1 = $request->kolektan_1;
        $data->kolektan_2 = $request->kolektan_2;
        $data->penerima_tamu_1 = $request->penerima_tamu_1;
        $data->penerima_tamu_2 = $request->penerima_tamu_2;    
        $data->update();
        
        return redirect(route('jPelayanan.sore'));

    }
}
