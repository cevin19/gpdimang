<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
// model yang digunakan
use App\Model\jadwal_ibadah;

class jadwalIbadahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $datas = jadwal_ibadah::all();
        return view('admin.jadwalIbadah.index',compact('datas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.jadwalIbadah.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \Carbon\Carbon::setLocale('id');
        $teaTime = Carbon::createFromTimeString($request->waktu, 'Asia/Jakarta');
// Sabtu, 04 Maret 2017 07:38
        // return $request->waktu;
        // $tes = date('D, d-m-Y', strtotime($request->waktu));

        // return $tes;
        if($request->gambar=='image_1.jpg'||'image_2.jpg'||'image_3.jpg'||'image_4.jpg'){
            $data   = new jadwal_ibadah;
            $data->judul    =   $request->judul;
            $data->atas_nama    =   $request->atas_nama;
            $data->waktu    =   $teaTime;
            $data->tempat    =   $request->tempat;
            $data->gambar    =   $request->gambar;
            $data->save();
            return redirect(route('admin-jadwal-ibadah.index'));
        }else{
            return 'belum bisa update gambar sendiri';
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    $date = Carbon::now();

    return $date->format('g:i a l jS F Y'); // 3:45 дня п’ятниця 16-го березня 2018

        $data= jadwal_ibadah::find($id);
        return view('admin.jadwalIbadah.show',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jadwal_ibadah= jadwal_ibadah::find($id);
        $data= jadwal_ibadah::find($id);
        return view('admin.jadwalIbadah.edit',compact('jadwal_ibadah','data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if($request->gambar=='image_1.jpg'||'image_2.jpg'||'image_3.jpg'||'image_4.jpg'){
            $data   =  jadwal_ibadah::find($id);
            $data->judul    =   $request->judul;
            $data->atas_nama    =   $request->atas_nama;
            $data->waktu    =   $request->waktu;
            $data->tempat    =   $request->tempat;
            $data->gambar    =   $request->gambar;
            $data->update();
            return redirect(route('admin-jadwal-ibadah.index'));
        }else{
            return 'belum bisa update gambar sendiri';
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        jadwal_ibadah::find($id)->delete();
        return redirect()->back();
    }
}
