<?php

Route::namespace('Admin')->middleware('auth')->group(function(){
    Route::get('admin/home',function(){
        return view('admin.home');
    })->name('admin.home');
    // pelayan ibadah
    route::get('jadwal-pelayanan-pagi','jadwalPelayananController@pagi')->name('jPelayanan.pagi');
    route::get('jadwal-pelayanan-siang','jadwalPelayananController@siang')->name('jPelayanan.siang');
    route::get('jadwal-pelayanan-sore','jadwalPelayananController@sore')->name('jPelayanan.sore');
    route::patch('jadwal-pelayanan-pagi/{id}','jadwalPelayananController@updatePagi')->name('jPelayananPagi.update');
    route::patch('jadwal-pelayanan-siang/{id}','jadwalPelayananController@updateSiang')->name('jPelayananSiang.update');
    route::patch('jadwal-pelayanan-sore/{id}','jadwalPelayananController@updateSore')->name('jPelayananSore.update');
    // galeri
    route::resource('galeri','galeriController');
    // route::resource('pagi','ibadahPagiController');
    route::resource('pelayan','PelayanController');
    route::resource('pelayanan','pelayananController');

    // pembacaan firman minggu lalu
    Route::resource('pembacaan-firman','firmanMingguController');

    // Jadwal Ibadah
    Route::resource('admin-jadwal-ibadah','jadwalIbadahController');
});

Route::namespace('User')->group(function () {
    Route::get('/','navigationController@beranda')->name('user.beranda');
    Route::get('/tentang-gereja','navigationController@tentangGereja')->name('user.tentangGereja');
    Route::get('/jadwal-ibadah','navigationController@jadwalIbadah')->name('user.jadwalIbadah');
    Route::get('/jadwal-pelayanan','navigationController@jadwalPelayanan')->name('user.jadwalPelayanan');
    Route::get('/Galeri','navigationController@galeri')->name('user.galeri');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
