@extends('admin.layouts.app')
@section('styleSection')
<style>
    input{
  width: 100%
}
td ul li{
  list-style: none;
  margin-left:-20px;
}
.btn-ubah{
  position: relative;
  float: right;
}
</style>
@endsection
@section('content')
<div id="content-wrapper">

    <div class="container-fluid">
        <!-- DataTables Example -->
        <div class="card mb-3">
            <div class="card-header">
                <i class="fas fa-table"></i>
                Jadwal Pelayanan Ibadah II - Siang</div>
            <div class="card-body">
                <div class="table-responsive">
                    <form action="{{route('jPelayananSiang.update',$data->id)}}" method="POST">
                        @csrf @method('PATCH')
                        <table class="table table-bordered" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Worship Leader</th>
                                    <th>Pemain Musik</th>
                                    <th>Multimedia</th>
                                    <th>Singer</th>
                                    <th>Kolektan</th>
                                    <th>Penerima Tamu</th>
                                </tr>
                            </thead>
                            {{-- <tfoot>
                                <tr>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Office</th>
                                    <th>Age</th>
                                    <th>Start date</th>
                                    <th>Salary</th>
                                </tr>
                            </tfoot> --}}
                            <tbody>
                                <tr>
                                    <td><input name="whorship_leader" value="{{$data->whorship_leader}}" type="text" list="listWL"></td>
                                    <td>
                                        <ul>
                                            <li><input name="pemain_musik_1" type="text" value="{{$data->pemain_musik_1}}" list="listMusik"></li>
                                            <li><input name="pemain_musik_2" type="text" value="{{$data->pemain_musik_2}}" list="listMusik"></li>
                                            <li><input name="pemain_musik_3" type="text" value="{{$data->pemain_musik_3}}" list="listMusik"></li>
                                            <li><input name="pemain_musik_4" type="text" value="{{$data->pemain_musik_4}}" list="listMusik"></li>
                                        </ul>
                                    </td>
                                    <td><input name="multimedia" type="text" value="{{$data->multimedia}}" list="listMultimedia"></td>
                                    <td>
                                        <ul>
                                            <li><input name="singer_1" type="text" value="{{$data->singer_1}}" list="listSinger"></li>
                                            <li><input name="singer_2" type="text" value="{{$data->singer_2}}" list="listSinger"></li>
                                            <li><input name="singer_3" type="text" value="{{$data->singer_3}}" list="listSinger"></li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul>
                                            <li><input name="kolektan_1" type="text" value="{{$data->kolektan_1}}" list="listKolektan"></li>
                                            <li><input name="kolektan_2" type="text" value="{{$data->kolektan_2}}" list="listKolektan"></li>
                                        </ul>
                                    </td>
                                    <td>
                                        <ul>
                                            <li><input name="penerima_tamu_1" type="text" value="{{$data->penerima_tamu_1}}" list="listPenerima"></li>
                                            <li><input name="penerima_tamu_2" type="text" value="{{$data->penerima_tamu_2}}" list="listPenerima"></li>
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <button type="submit" class="btn-ubah btn btn-success">Ubah</button>
                    </form>
                </div>
            </div>
            @if($data->updated_at!=null)
            <div class="card-footer small text-muted">{{$data->updated_at->diffForHumans()}}</div>
            @endif
        </div>
    </div>
</div>
<datalist id="listWL">
    @foreach($wls as $wl)
    @foreach($wl->pelayans as $wlPelayan)
    <option value="{{$wlPelayan->name}}">{{$wlPelayan->name}}</option>
    @endforeach
    @endforeach
</datalist>
<datalist id="listMultimedia">
    @foreach($multimedias as $multimedia)
    @foreach($multimedia->pelayans as $multimediaPelayan)
    <option value="{{$multimediaPelayan->name}}">{{$multimediaPelayan->name}}</option>
    @endforeach
    @endforeach
</datalist>
<datalist id="listMusik">
    @foreach($musiks as $musik)
    @foreach($musik->pelayans as $musikPelayan)
    <option value="{{$musikPelayan->name}}">{{$musikPelayan->name}}</option>
    @endforeach
    @endforeach
</datalist>
<datalist id="listSinger">
    @foreach($singers as $singer)
    @foreach($singer->pelayans as $singerPelayan)
    <option value="{{$singerPelayan->name}}">{{$singerPelayan->name}}</option>
    @endforeach
    @endforeach
</datalist>
<datalist id="listKolektan">
    @foreach($kolektans as $kolektan)
    @foreach($kolektan->pelayans as $kolektanPelayan)
    <option value="{{$kolektanPelayan->name}}">{{$kolektanPelayan->name}}</option>
    @endforeach
    @endforeach
</datalist>
<datalist id="listPenerima">
    @foreach($penerimas as $penerima)
    @foreach($penerima->pelayans as $penerimaPelayan)
    <option value="{{$penerimaPelayan->name}}">{{$penerimaPelayan->name}}</option>
    @endforeach
    @endforeach
</datalist>
@endsection
