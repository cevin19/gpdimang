@extends('admin.layouts.app')
@section('styleSection')
    <style>
    .height-up{
        height: 500px;
    }
    </style>
@endsection
@section('content')
<div id="content-wrapper">

    <div class="container-fluid">
        @if(count($errors) > 0)
        @foreach($errors->all() as $error)
        <span class="alert alert-danger" style="padding:100px">{{$error}}</span>
        @endforeach
        @endif
        <div class="card mb-3">
            <div class="card-header">Tambah Album Baru</div>
            <form style="padding:10px" method="POST" action="{{route('galeri.store')}}">
                @csrf
                <div class="form-group">
                    <label for="exampleFormControlInput1">Judul Album</label>
                    <input name="nama" type="text" class="form-control" placeholder="Judul Album">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Link Foto Depan</label>
                    <input name="foto_depan" type="text" class="form-control" placeholder="Link foto depan">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlInput1">Link Galeri</label>
                    <input name="link" type="text" class="form-control" placeholder="Link Galeri">
                </div>
                <button class="btn btn-success" type="submit">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection
