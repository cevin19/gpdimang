@extends('admin.layouts.app')
@section('content')
<div id="content-wrapper">

    <div class="container-fluid">
<!-- DataTables Example -->
<div class="card mb-3">
    <div class="card-header">
      <i class="fas fa-table"></i>
      Daftar Pelayan
    <span><a href="{{route('pelayan.create')}}" class="btn btn-success float-right">Tambah</a></span>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th width="90%">Nama</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tfoot>
            <tr>
              <th>Nama</th>
              <th>Aksi</th>
              
            </tr>
          </tfoot>
          <tbody>
            @foreach($pelayans as $pelayan)
            <tr>
              <td>{{$pelayan->name}}</td>  
              <td>
                  <a href="{{route('pelayan.edit',$pelayan->id)}}"><span class="fas fa-edit fa-lg"></span></a>
                                    <form id="form-delete-{{$pelayan->id}}" action="{{route('pelayan.destroy',$pelayan->id)}}"
                                        method="POST" style="display:none">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                    <a href="#" onclick="
                    if(confirm('yakin ingin menghapus?')){
                        event.preventDefault();document.getElementById('form-delete-{{$pelayan->id}}').submit();
                    }else{
                        event.preventDefault();
                    }
                    "><span
                                            class="fas fa-trash-alt fa-lg" style="color:#ff4907"></span></a>
                                
            </td>                  
            </tr>
            @endforeach
            
          </tbody>
        </table>
      </div>
    </div>
    <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
  </div>
    </div>
</div>
@endsection