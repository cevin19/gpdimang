@extends('admin.layouts.app')
@section('styleSection')
    <style>
    .height-up{
        height: 500px;
    }
    </style>
@endsection
@section('content')
<div id="content-wrapper">

    <div class="container-fluid">
        @if(count($errors) > 0)
        @foreach($errors->all() as $error)
        <span class="alert alert-danger" style="padding:100px">{{$error}}</span>
        @endforeach
        @endif
        <div class="card mb-3">
            <div class="card-header">Tambah Pelayanan Baru</div>
            <form style="padding:10px" method="POST" action="{{route('pelayan.store')}}">
                @csrf
                <div class="form-group">
                    <label for="exampleFormControlInput1">Nama</label>
                    <input name="name" type="text" class="form-control" placeholder="Nama">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect2">Pelayanan</label>
                    <select name="jenis_pelayanan[]" multiple class="form-control height-up" id="exampleFormControlSelect2" >
                        @foreach ($jenis_pelayanan as $data)
                        <option value="{{$data->id}}">{{$data->nama}}</option>
                        @endforeach
                    </select>
                </div>
                <button class="btn btn-success" type="submit">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection
