@extends('admin.layouts.app')
@section('content')
<div id="content-wrapper">

    <div class="container-fluid">
        @if(count($errors) > 0)
        @foreach($errors->all() as $error)
        <span class="alert alert-danger" style="padding:100px">{{$error}}</span>
        @endforeach
        @endif
        <div class="card mb-3">
            <div class="card-header">Tambah Pelayanan Baru</div>
            <form style="padding:10px" method="POST" action="{{route('pelayan.update',$pelayan->id)}}">
                @csrf @method('PATCH')
                <div class="form-group">
                    <label for="exampleFormControlInput1">Nama</label>
                    <input name="name" type="text" class="form-control" placeholder="Nama" value="{{$pelayan->name}}">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect2">Pelayanan</label>
                    <select name="jenis_pelayanan[]" multiple class="form-control" id="exampleFormControlSelect2">
                        @foreach ($jenis_pelayanan as $data)
                        <option value="{{$data->id}}"
                            @foreach($pelayan->pelayanans as $pelayanPelayanan)
                                @if($pelayanPelayanan->id == $data->id)
                                selected
                                @endif
                            @endforeach>{{$data->nama}}</option>
                        @endforeach
                    </select>
                </div>
                <button class="btn btn-success" type="submit">Ubah</button>
            </form>
        </div>
    </div>
</div>
@endsection
