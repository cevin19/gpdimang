@extends('admin.layouts.app')
@section('content')
<div id="content-wrapper">

    <div class="container-fluid">
<!-- DataTables Example -->
<div class="card mb-3">
    <div class="card-header">
      
     Tambah Pelayanan
     
    </div>
    <div class="card-body">
       @include('pesan.peringatan')
        <form action="{{route('pelayanan.update',$data->id)}}" method="POST">
            @csrf
            @method('PATCH')
            <div class="col-md-8 row mx-auto">
                <label class="col-md-3" for="Pelayanan">Pelayanan</label>
                <input class="form-control col-md-5" type="text" name="nama" id="" value="{{$data->nama}}">
            </div><br>
            <div class="col-md-3 mx-auto">
                <button class="btn btn-success mx-auto" type="submit">Tambah</button>
                <a class="btn btn-warning mx-auto" href="{{route('pelayanan.index')}}">Kembali</a>
            </div>
        </form>
    </div>
</div>
    </div>
</div>
@endsection