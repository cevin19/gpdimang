@extends('admin.layouts.app')
@section('content')
<div id="content-wrapper">

    <div class="container-fluid">
        <!-- DataTables Example -->
        <div class="card mb-3">
            <div class="card-header">
                <i class="fas fa-table"></i>
                Daftar Pelayanan
                <a href="{{route('pelayanan.create')}}" class="btn btn-success float-right">Tambah</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th style="width:10%">No</th>
                                <th>Pelayanan</th>
                                <th style="width:10%">Aksi</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>No</th>
                                <th>Pelayanan</th>
                                <th>Aksi</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach ($datas as $data)

                            <tr>
                                <td>{{$loop->index + 1}}</td>
                                <td>{{$data->nama}}</td>
                                <td>
                                    <a href="{{route('pelayanan.edit',$data->id)}}"><span class="fas fa-edit fa-lg"></span></a>
                                    <form id="form-delete-{{$data->id}}" action="{{route('pelayanan.destroy',$data->id)}}"
                                        method="POST" style="display:none">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                    <a href="#" onclick="
                    if(confirm('yakin ingin menghapus?')){
                        event.preventDefault();document.getElementById('form-delete-{{$data->id}}').submit();
                    }else{
                        event.preventDefault();
                    }
                    "><span
                                            class="fas fa-trash-alt fa-lg" style="color:#ff4907"></span></a>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>
    </div>
</div>
@endsection
