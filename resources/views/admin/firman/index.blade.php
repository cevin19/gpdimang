@extends('admin.layouts.app')
@section('content')
<div id="content-wrapper">

    <div class="container-fluid">
<!-- DataTables Example -->
<div class="card mb-3">
    <div class="card-header">
      <i class="fas fa-table"></i>
     Pembacaan Firman
    {{-- <span><a href="{{route('pelayan.create')}}" class="btn btn-success float-right">Tambah</a></span> --}}
    </div>
    <div class="card-body">
        <form action="{{route('pembacaan-firman.update',$data->id)}}" method="POST">
            @csrf @method('PATCH')
            <div class="form-group">
              <label for="formGroupExampleInput">Judul Khotbah</label>
              <input name="judul" type="text" class="form-control" id="formGroupExampleInput" placeholder="Masukan Data" value="{{$data->judul}}">
            </div>
            <div class="form-group">
              <label for="formGroupExampleInput2">Pembawa Firman</label>
              <input name="pengkhotbah" type="text" class="form-control" id="formGroupExampleInput2" placeholder="Masukan Data" value="{{$data->pengkhotbah}}">
            </div>
            <div class="form-group">
              <label for="formGroupExampleInput2">Ayat Pokok</label>
              <input name="ayat" type="text" class="form-control" id="formGroupExampleInput2" placeholder="Masukan Data" value="{{$data->ayat}}">
            </div>
            <div class="form-group">
              <label for="formGroupExampleInput2">Isi Firman</label>
              <textarea name="isi" class="form-control" id="exampleFormControlTextarea1" rows="3">{{$data->isi}}</textarea>
            </div>
            <button onclick="alert('berhasil diubah!')" type="submit" class="btn-ubah btn btn-success">Ubah</button>
          </form>
    </div>
    @if($data->updated_at!=null)
            <div class="card-footer small text-muted">{{$data->updated_at->diffForHumans()}}</div>
            @endif
  </div>
    </div>
</div>
@endsection