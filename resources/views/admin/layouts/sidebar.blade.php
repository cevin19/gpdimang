<!-- Sidebar -->
<ul class="sidebar navbar-nav">
    <li class="nav-item active">
      <a class="nav-link" href="{{route('admin.home')}}">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span>
      </a>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-fw fa-folder"></i>
        <span>Jadwal Pelayanan</span>
      </a>
      <div class="dropdown-menu" aria-labelledby="pagesDropdown">
        <h6 class="dropdown-header">Waktu</h6>
        <a class="dropdown-item" href="{{route('jPelayanan.pagi')}}">Ibadah Pagi</a>
        <a class="dropdown-item" href="{{route('jPelayanan.siang')}}">Ibadah Siang</a>
        <a class="dropdown-item" href="{{route('jPelayanan.sore')}}">Ibadah Sore</a>
      </div>
      <li class="nav-item">
        <a class="nav-link" href="{{route('admin-jadwal-ibadah.index')}}">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Jadwal Ibadah</span></a>
      </li>
      {{-- <div class="dropdown-menu" aria-labelledby="pagesDropdown">
        <h6 class="dropdown-header">Login Screens:</h6>
        <a class="dropdown-item" href="login.html">Login</a>
        <a class="dropdown-item" href="register.html">Register</a>
        <a class="dropdown-item" href="forgot-password.html">Forgot Password</a>
        <div class="dropdown-divider"></div>
        <h6 class="dropdown-header">Other Pages:</h6>
        <a class="dropdown-item" href="404.html">404 Page</a>
        <a class="dropdown-item" href="blank.html">Blank Page</a>
      </div> --}}
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{route('pelayan.index')}}">
        <i class="fas fa-fw fa-chart-area"></i>
        <span>Daftar Pelayan</span></a>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{route('pembacaan-firman.show',1)}}">
          <i class="fas fa-fw fa-table"></i>
          <span>Pembacaan Firman</span></a>
        </li>
    <li class="nav-item">
      <a class="nav-link" href="{{route('galeri.index')}}">
        <i class="fas fa-fw fa-chart-area"></i>
        <span>Galeri</span></a>
    </li>
  </ul>