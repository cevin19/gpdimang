<!DOCTYPE html>
<html lang="en">

  <head>

    @include('admin.layouts.head')
  </head>

  <body id="page-top">

    @include('admin.layouts.navbar')

    <div id="wrapper">

      <!-- Sidebar -->
      @include('admin.layouts.sidebar')

      @section('content')
        @show

    </div>
    <!-- /#wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fas fa-angle-up"></i>
    </a>

    @section('modal')
    @show

    @include('admin.layouts.footer')
    
  </body>

</html>
