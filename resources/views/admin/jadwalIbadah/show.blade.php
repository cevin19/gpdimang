@extends('admin.layouts.app')
@section('styleSection')
    <style>
    .height-up{
        height: 500px;
    }
    </style>
@endsection
@section('content')
<div id="content-wrapper">

    <div class="container-fluid">
        @if(count($errors) > 0)
        @foreach($errors->all() as $error)
        <span class="alert alert-danger" style="padding:100px">{{$error}}</span>
        @endforeach
        @endif
        <div class="card mb-3">
            <div class="card-header">Jadwal Ibadah</div>
            
            
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="exampleFormControlInput1">Judul</label>
                        <input name="judul" type="text" class="form-control" placeholder="Jenis Ibadah" value="{{$data->judul}}">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleFormControlInput1">Atas Nama</label>
                        <input name="atas_nama" type="text" class="form-control" placeholder="Atas Nama" value="{{$data->atas_nama}}">
                    </div>
                </div>
                <div class="row">
                        <div class="form-group col-md-4">
                            <label for="exampleFormControlInput1">Waktu</label>
                            <input name="waktu" type="datetime-local" class="form-control" placeholder="waktu" value="{{$data->waktu}}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="exampleFormControlInput1">Tempat</label>
                            <input name="tempat" type="text" class="form-control" placeholder="Lokasi" value="{{$data->tempat}}">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="exampleFormControlInput1" >Gambar</label>
                            <input name="gambar" type="text" class="form-control" list="listGambar" placeholder="Gambar" value="{{$data->gambar}}">
                        </div>
                    </div>
                
                    <a href="{{route('admin-jadwal-ibadah.index')}}" class="btn btn-warning col-md-2 mx-auto">Kembali</a>
            </form>
        </div>
    </div>
</div>
<datalist id="listGambar">
    <option value="image_1.jpg">Gambar 1</option>
    <option value="image_2.jpg">Gambar 2</option>
    <option value="image_3.jpg">Gambar 3</option>
    <option value="image_4.jpg">Gambar 4</option>
</datalist>
@endsection
