@extends('admin.layouts.app')
@section('styleSection')
    <style>
    .height-up{
        height: 500px;
    }
    </style>
@endsection
@section('content')
<div id="content-wrapper">

    <div class="container-fluid">
        @if(count($errors) > 0)
        @foreach($errors->all() as $error)
        <span class="alert alert-danger" style="padding:100px">{{$error}}</span>
        @endforeach
        @endif
        <div class="card mb-3">
            <div class="card-header">Tambah Jadwal Ibadah</div>
            <form  style="padding:10px" method="POST" action="{{route('admin-jadwal-ibadah.store')}}">
                @csrf
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="exampleFormControlInput1">Judul</label>
                        <input name="judul" type="text" class="form-control" placeholder="Jenis Ibadah">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleFormControlInput1">Atas Nama</label>
                        <input name="atas_nama" type="text" class="form-control" placeholder="Atas Nama">
                    </div>
                </div>
                <div class="row">
                        <div class="form-group col-md-4">
                            <label for="exampleFormControlInput1">Waktu</label>
                            <input name="waktu" type="datetime-local" class="form-control" placeholder="Nama">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="exampleFormControlInput1">Tempat</label>
                            <input name="tempat" type="text" class="form-control" placeholder="Lokasi">
                        </div>
                        <div class="form-group col-md-4">
                            <label for="exampleFormControlInput1" >Gambar</label>
                            <input name="gambar" type="text" class="form-control" list="listGambar" placeholder="Gambar">
                        </div>
                    </div>
                <button class="btn btn-success" type="submit">Submit</button>
            </form>
        </div>
    </div>
</div>
<datalist id="listGambar">
    <option value="image_1.jpg">Gambar 1</option>
    <option value="image_2.jpg">Gambar 2</option>
    <option value="image_3.jpg">Gambar 3</option>
    <option value="image_4.jpg">Gambar 4</option>
    <option value="ibadah-pemuda.jpg">Ibadah Pemuda</option>
    <option value="ibadah-komsel.jpg">Ibadah Komsel</option>
    <option value="kaum-wanita.jpg">Kaum Wanita</option>
    <option value="kaum-pria.jpg">Kaum Pria</option>
    <option value="doa-pagi.jpg">Doa Pagi</option>
</datalist>
@endsection
