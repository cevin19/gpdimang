@extends('admin.layouts.app')
@section('content')
<div id="content-wrapper">

    <div class="container-fluid">
        <!-- DataTables Example -->
        <div class="card mb-3">
            <div class="card-header">
                <i class="fas fa-table"></i>
                Jadwal Ibadah
                <span><a href="{{route('admin-jadwal-ibadah.create')}}" class="btn btn-success float-right">Tambah</a></span>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                
                                <th>Judul</th>
                                <th>Atas Nama</th>
                                <th>Waktu</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                
                                <th>Judul</th>
                                <th>Atas Nama</th>
                                <th>Waktu</th>
                                <th>Aksi</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($datas as $data)
                            <tr>
                                <td>{{$data->judul}}</td>
                                <td>{{$data->atas_nama}}</td>
                                <td>{{$data->waktu}}</td>
                                <td>
                                    <a href="{{route('admin-jadwal-ibadah.show',$data->id)}}"><span class="fas fa-eye fa-lg" style="color:#1cbc28"></span></a>
                                    <a href="{{route('admin-jadwal-ibadah.edit',$data->id)}}"><span class="fas fa-edit fa-lg"></span></a>
                                    <form id="form-delete-{{$data->id}}" action="{{route('admin-jadwal-ibadah.destroy',$data->id)}}"
                                        method="POST" style="display:none">
                                        @csrf
                                        @method('DELETE')
                                    </form>
                                    <a href="#" onclick="
                    if(confirm('yakin ingin menghapus?')){
                        event.preventDefault();document.getElementById('form-delete-{{$data->id}}').submit();
                    }else{
                        event.preventDefault();
                    }
                    "><span
                                            class="fas fa-trash-alt fa-lg" style="color:#ff4907"></span></a>

                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>
    </div>
</div>
@endsection
