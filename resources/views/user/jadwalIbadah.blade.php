@extends('user.layouts.app')
@section('content')

<section class="ftco-section">
    <div class="container">
      <div class="row">
        @foreach($datas as $data)
        <div class="col-md-4 ftco-animate fadeInUp ftco-animated">
          <div class="blog-entry">
            <a href="#" class="block-20" style="background-image:url({{URL::asset('user/images/'.$data->gambar)}});">
            </a>
            <div class="text p-4 d-block">
              <div class="meta mb-3">
                
                <div><a href="#">{{$weekMap[\Carbon\Carbon::parse($data->waktu)->dayOfWeek]}}, {{\Carbon\Carbon::parse($data->waktu)->format('d/m/y H:i')}}</a></div> 
                {{-- <div><a href="#">{{date('l, d/m/y H:i', strtotime($data->waktu))}}</a></div>  --}}
                <div><a href="#">
                 
                  </a></div> 
                <div>- <a href="#">{{$data->tempat}}</a></div>
                {{-- <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div> --}}
              </div>
              <h3 class="heading"><a href="#">{{$data->judul}}</a></h3>
              <h6 class="heading" style="font-size:15px;"><a href="#">Atas Nama: {{$data->atas_nama}}</a></h6>
            </div>
          </div>
        </div>
        @endforeach
  
      </div>
    </div>
  </section>
@endsection