@extends('user.layouts.app')
@section('head-section')
<style>
    .sejarah-hp {
        display: none;
    }

    .foto-profil-gembala {
        width: 50%;
        height: 550px;
    }

    @media (max-width : 755px) {
        .sejarah-hp {
            display: block;
        }

        .sejarah-pc {
            display: none;
        }

        .foto-profil-gembala {
            width: 100%;
        }
    }

</style>
@endsection
@section('content')
<section class="ftco-section-2">
    <div class="container-fluid">
        <div class="section-2-blocks-wrapper d-flex row no-gutters">
            <div class="img col-md-6 ftco-animate fadeInUp ftco-animated" style="background-image: url('{{URL::asset('user/images/sejarah-gereja_1.jpg')}}');">
                {{-- <a href="https://vimeo.com/45830194" class="button popup-vimeo"><span class="ion-ios-play"></span></a>
                --}}
            </div>
            <div class="text col-md-6 ftco-animate fadeInUp ftco-animated">
                <div class="text-inner align-self-start">

                    <h3>Tentang Gereja</h3>
                    <p>GPdI Manggar lahir dari sebuah pengembangan pelayanan(Church Planting) GPdI Rapak-Balikpapan
                        yang pada saat itu digembalakan oleh Pdt. M.S Tampongangoy. Beliau menugaskan seorang pengerja
                        pakteknya yaitu Sdr. Styven Karamoy untuk merintis Pelayanan di Manggar dan kegiatan Ibadah
                        mula-mula dilakukan di sebuah rumah jemaat(Gg. Wahid) dengan beberapa anggota jemaat...</p>
                    {{-- <p>September 1997 Ibadah mulai dilaksanakan seminggu sekali pada setiam hari sabtu malam, dan
                        sore
                        harinya dilakukan ibadah anak-anak(sek.Minggu), Puji Tuhan jiwa-jiwa banyak yang tertolong...
                        Seiring berjalannya waktu dinamika pelayanan Gpdi Manggar mulai terasa, pergumulan demi
                        pergumulan harus dilewati, pengorbanan maupun perjuangan serta air mata selalu mewarnai
                        pelayanan ini...., tetapi Tuhan tahu bagaimana menghibur & membela pekerjaan-Nya.</p> --}}
                    {{-- <p>Januari 2001 oleh kuasa kemurahan Tuhan kami memperoleh lahan untuk membangun sebuah
                        Gereja. Semenjak itu usaha untuk mendapatkan izin membangun oleh pemerintah kami lakukan...</p>
                    <p>Februari 2003 Ijin Mendirikan Bangunan(IMB) diberikan. Setelah mendapatkan ijin dari Wali kota
                        Balikpapan, dan dengan bermodal Iman perletakan batu pertama Gedung Gereja dengan ukuran 10 x
                        22.1/2 dimulai.</p>
                    <p>Desember 2007, gedung Gereja GPdI manggar dapat berdiri, dan hari ini selasa 14 Juli 2008 Oleh
                        kemurahan Tuhan Gereja GPdI Manggar dapat diresmikan, Segala Puji hanya bagi Tuhan
                        Yesus......... Haleluyah!!!</p> --}}

                    <p></p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="ftco-section-2 sejarah-hp">
    <div class="container-fluid">
        <div class="section-2-blocks-wrapper d-flex row no-gutters">
            <div class="img col-md-6 ftco-animate fadeInUp ftco-animated" style="background-image: url('{{URL::asset('user/images/sejarah-gereja_2.jpg')}}');height:550px">
            </div>
            <div class="text col-md-6 ftco-animate fadeInUp ftco-animated">
                <div class="text-inner align-self-start">

                    <p>Januari 2001 oleh kuasa kemurahan Tuhan kami memperoleh lahan untuk membangun sebuah Gereja.
                        Semenjak itu usaha untuk mendapatkan izin membangun oleh pemerintah kami lakukan...</p>
                    <p>Februari 2003 Ijin Mendirikan Bangunan(IMB) diberikan. Setelah mendapatkan ijin dari Wali kota
                        Balikpapan, dan dengan bermodal Iman perletakan batu pertama Gedung Gereja dengan ukuran 10 x
                        22.1/2 dimulai.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="ftco-section-2 sejarah-pc">
    <div class="container-fluid">
        <div class="section-2-blocks-wrapper d-flex row no-gutters">
            <div class="text col-md-6 ftco-animate fadeInUp ftco-animated">
                <div class="text-inner align-self-start">
                    <p>Januari 2001 oleh kuasa kemurahan Tuhan kami memperoleh lahan untuk membangun sebuah Gereja.
                        Semenjak itu usaha untuk mendapatkan izin membangun oleh pemerintah kami lakukan...</p>
                    <p>Februari 2003 Ijin Mendirikan Bangunan(IMB) diberikan. Setelah mendapatkan ijin dari Wali kota
                        Balikpapan, dan dengan bermodal Iman perletakan batu pertama Gedung Gereja dengan ukuran 10 x
                        22.1/2 dimulai.</p>
                </div>
            </div>
            <div class="img col-md-6 ftco-animate fadeInUp ftco-animated" style="background-image: url('{{URL::asset('user/images/sejarah-gereja_2.jpg')}}');height:550px">
            </div>
        </div>
    </div>
</section>
<section class="ftco-section-2">
    <div class="container-fluid">
        <div class="section-2-blocks-wrapper d-flex row no-gutters">
            <div class="img col-md-6 ftco-animate fadeInUp ftco-animated" style="background-image: url('{{URL::asset('user/images/sejarah-gereja_3.jpg')}}');height:550px">
                {{-- <a href="https://vimeo.com/45830194" class="button popup-vimeo"><span class="ion-ios-play"></span></a>
                --}}
            </div>
            <div class="text col-md-6 ftco-animate fadeInUp ftco-animated">
                <div class="text-inner align-self-start">

                    <p>Desember 2007, gedung Gereja GPdI manggar dapat berdiri, dan hari ini selasa 14 Juli 2008 Oleh
                        kemurahan Tuhan Gereja GPdI Manggar dapat diresmikan, Segala Puji hanya bagi Tuhan
                        Yesus......... Haleluyah!!!</p>

                    <p></p>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- section visi misi--}}
<div class="row justify-content-center mt-5 container-fluid">
    <div class="col-md-6 text-center heading-section ftco-animate fadeInUp ftco-animated">
        <h2 class="mb-4">Visi & Misi Gereja</h2>
    </div>
</div>
<section class="ftco-section-3 bg-light">
    <div class="container">
        <div class="row no-gutters">
            <div class="col-md-3 py-5 nav-link-wrap aside-stretch" style="height:550px">
                <div class="nav ftco-animate flex-column nav-pills text-md-right fadeInUp ftco-animated" id="v-pills-tab"
                    role="tablist" aria-orientation="vertical">
                    <a onclick="visi()" class="nav-link pr-5 active" id="v-pills-vission-tab" data-toggle="pill" href="#v-pills-buffet"
                        role="tab" aria-controls="v-pills-vission" aria-selected="true">Visi</a>
                    <a onclick="misi()" class="nav-link  pr-5" id="v-pills-mission-tab" data-toggle="pill" href="#v-pills-master"
                        role="tab" aria-controls="v-pills-mission" aria-selected="false">Misi</a>

                </div>
            </div>

            <div class="col-md-9 pt-5 pb-5 pl-md-5 d-flex align-items-center" style="height:550px">

                <div class="tab-content ftco-animate pl-md-5 fadeInUp ftco-animated " id="v-pills-tabContent">

                    <div class="tab-pane fade  " id="v-pills-mission" role="tabpanel" aria-labelledby="v-pills-mission-tab">
                        <span class="icon mb-3 d-block flaticon-bed"></span>
                        <h2 class="mb-4">Misi Gereja GPDI Manggar</h2>
                        <p class="lead">Menjangkau jiwa, Membangun persekutuan dan menjadi berkat untuk sesama dan
                            semua orang.</p>

                    </div>

                    <div class="tab-pane fade show active" id="v-pills-vission" role="tabpanel" aria-labelledby="v-pills-vission-tab">
                        <span class="icon mb-3 d-block flaticon-tray"></span>
                        <h2 class="mb-4">Visi Gereja GPDI Manggar</h2>
                        <p class="lead">"Menjadi Jemaat yang mengasihi & melayani Tuhan serta berdampak bagi semua
                            orang."</p>

                        {{-- <p><a href="#" class="btn btn-primary">Learn More</a></p> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- section profil gembala --}}
<div class="row justify-content-center mt-5 container-fluid">
    <div class="col-md-6 text-center heading-section ftco-animate fadeInUp ftco-animated">
        <h2 class="mb-4">Profil Gembala</h2>
    </div>
</div>
<section class="ftco-section-2 ">
    <div class="container-fluid">
        <div class="section-2-blocks-wrapper d-flex row no-gutters">
            <div class="img col-md-6 ftco-animate fadeInUp ftco-animated" style="background-image: url('{{URL::asset('user/images/foto-profil-gembala.jpg')}}');height:550px">
            </div>
            <div class="text col-md-6 ftco-animate fadeInUp ftco-animated" style="padding:25px">
                <div class="text-inner align-self-start">
                    <p>Meski lahir dari keluarga Hamba Tuhan yang sederhana tetapi tidak pernah menolak panggilan Tuhan
                        atas hidupnya. Memulai pendidikan Teologia di sekolah Alkitab Balikpapan dan melanjutkan di
                        Jember Biblle Collage (JBC) sekarang STA Jember, kini Tuhan mempercayakan banyak pelayanan
                        Gereja baik pada organisasi gereja lokal maupun interdenominasi Gereja di Balikpapan dan
                        Kalimantan timur serta Nasional.</p>
                    <p> Komitmen serta penyerahan diri dan tekat yang kuat adalah
                        modal awal ketika memulai sebuah pelayanan perintisan jemaat baru di kota Balikpapan dan hingga
                        kini menjadi seorang Gembala jemaat di GPdI Manggar Balikpapan dan Guru di Sekolah Alkitab
                        Balikpapan. Melayani bersama Istri yaitu Joen Merry sudah dikaruniai dua orang anak yaitu
                        Clarissa Juditha Margareth Karamoy dan Andrew Matthew Karamoy. Selain menjadi Trainner dan
                        pembicara dalam berbagai event Pemuda juga adalah seorang wakil sekretaris Komisi Pusat Pemuda
                        GPdI dan anggota Departemen Remaja Pemuda PGPI Pusat.</p>
                    <p> Pdt. Styven Y. Karamoy adalah Pendiri
                        dari Yayasan Pancaran Kasih Borneo (YPKB) yang aktiv dalam lembaga pendidikan anak. </p>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    // var mission =document.getElementById('v-pills-mission-tab');
    // var vission =document.getElementById('v-pills-vission-tab');
    var pillmission = document.getElementById('v-pills-mission');
    var pillvission = document.getElementById('v-pills-vission');

    function visi() {
        pillmission.classList.remove('show', 'active');
        pillvission.classList.add('show', 'active');
    }

    function misi() {
        pillvission.classList.remove('show', 'active');
        pillmission.classList.add('show', 'active');
    }

</script>
@endsection
