@extends('user.layouts.app')
@section('body-style')
style="background-color:rgba(10,10,320,0.2)"
@endsection
@section('head-section')
<style>
    ul{
        list-style:none;
        padding-left:0px;
        margin-left:0px;
    }
</style>
@endsection
@section('content')
    <div class="col-md-12">
        <div class=" col-md-8 mx-auto">
            <div class="table-responsive" style="margin:20px 0px;">
                <h4 class="text-center">Ibadah Raya I</h4>
                <table class="table table-bordered bg-light" >
       
                    <!-- On rows -->
                    <thead class="bg-danger" style="color:white;">
                        <th>Worship Leader</th>
                        <th>Pemain Musik</th>
                        <th>Multimedia</th>
                        <th>Singer</th>
                        <th>Kolektan</th>
                        <th>Penerima Tamu</th>
                    </thead>
                    <tbody>
                        <tr class="">
                            {{-- wl --}}
                            <td>{{$pagi->whorship_leader}}</td>
                            {{-- musik --}}
                            <td>
                                <ul>
                                    <li>{{$pagi->pemain_musik_1}}</li>
                                    <li>{{$pagi->pemain_musik_2}}</li>
                                    <li>{{$pagi->pemain_musik_3}}</li>
                                    <li>{{$pagi->pemain_musik_4}}</li>
                                </ul>
                            </td>
                            {{-- multimedia --}}
                            <td>{{$pagi->multimedia}}</td>
                            {{-- singer --}}
                            <td><ul>
                                    <li>{{$pagi->singer_1}}</li>
                                    <li>{{$pagi->singer_2}}</li>
                                    <li>{{$pagi->singer_3}}</li>
                                </ul>
                            </td>
                            {{-- kolektan --}}
                            <td>
                                <ul>
                                    <li>{{$pagi->kolektan_1}}</li>
                                    <li>{{$pagi->kolektan_2}}</li>
                                </ul>
                            </td>
                            {{-- tamu --}}
                            <td>
                                <ul>
                                    <li>{{$pagi->penerima_tamu_1}}</li>
                                    <li>{{$pagi->penerima_tamu_2}}</li>
                                </ul>
                            </td>
                        </tr>
                    </tbody>

                </table>
              </div>

              {{-- ibadah raya II --}}
              <div class="table-responsive" style="margin:20px 0px;">
                    <h4 class="text-center">Ibadah Raya II</h4>
                    <table class="table table-bordered bg-light" >
           
                        <!-- On rows -->
                        <thead class="bg-success" style="color:white;">
                            <th>Worship Leader</th>
                            <th>Pemain Musik</th>
                            <th>Multimedia</th>
                            <th>Singer</th>
                            <th>Kolektan</th>
                            <th>Penerima Tamu</th>
                        </thead>
                        <tbody>
                                <tr class="">
                                    {{-- wl --}}
                                    <td>{{$siang->whorship_leader}}</td>
                                    {{-- musik --}}
                                    <td>
                                        <ul>
                                            <li>{{$siang->pemain_musik_1}}</li>
                                            <li>{{$siang->pemain_musik_2}}</li>
                                            <li>{{$siang->pemain_musik_3}}</li>
                                            <li>{{$siang->pemain_musik_4}}</li>
                                        </ul>
                                    </td>
                                    {{-- multimedia --}}
                                    <td>{{$siang->multimedia}}</td>
                                    {{-- singer --}}
                                    <td><ul>
                                            <li>{{$siang->singer_1}}</li>
                                            <li>{{$siang->singer_2}}</li>
                                            <li>{{$siang->singer_3}}</li>
                                        </ul>
                                    </td>
                                    {{-- kolektan --}}
                                    <td>
                                        <ul>
                                            <li>{{$siang->kolektan_1}}</li>
                                            <li>{{$siang->kolektan_2}}</li>
                                        </ul>
                                    </td>
                                    {{-- tamu --}}
                                    <td>
                                        <ul>
                                            <li>{{$siang->penerima_tamu_1}}</li>
                                            <li>{{$siang->penerima_tamu_2}}</li>
                                        </ul>
                                    </td>
                                </tr>
                            </tbody>
                    </table>
                  </div>

                  {{-- ibadah raya III --}}
                  <div class="table-responsive" style="margin:20px 0px;">
                        <h4 class="text-center">Ibadah Raya III</h4>
                        <table class="table table-bordered bg-light" >
               
                            <!-- On rows -->
                            <thead class="bg-warning" style="color:white;">
                                <th>Worship Leader</th>
                                <th>Pemain Musik</th>
                                <th>Multimedia</th>
                                <th>Singer</th>
                                <th>Kolektan</th>
                                <th>Penerima Tamu</th>
                            </thead>
                            <tbody>
                        <tr class="">
                            {{-- wl --}}
                            <td>{{$sore->whorship_leader}}</td>
                            {{-- musik --}}
                            <td>
                                <ul>
                                    <li>{{$sore->pemain_musik_1}}</li>
                                    <li>{{$sore->pemain_musik_2}}</li>
                                    <li>{{$sore->pemain_musik_3}}</li>
                                    <li>{{$sore->pemain_musik_4}}</li>
                                </ul>
                            </td>
                            {{-- multimedia --}}
                            <td>{{$sore->multimedia}}</td>
                            {{-- singer --}}
                            <td><ul>
                                    <li>{{$sore->singer_1}}</li>
                                    <li>{{$sore->singer_2}}</li>
                                    <li>{{$sore->singer_3}}</li>
                                </ul>
                            </td>
                            {{-- kolektan --}}
                            <td>
                                <ul>
                                    <li>{{$sore->kolektan_1}}</li>
                                    <li>{{$sore->kolektan_2}}</li>
                                </ul>
                            </td>
                            {{-- tamu --}}
                            <td>
                                <ul>
                                    <li>{{$sore->penerima_tamu_1}}</li>
                                    <li>{{$sore->penerima_tamu_2}}</li>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
        
                        </table>
                      </div>
        </div>
    </div>
@endsection