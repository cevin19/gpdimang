<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container fluid">
        <img src="{{asset('user/images/gpdi.png')}}" alt="" style="height:60px;"><a class="navbar-brand" href="{{route('user.beranda')}}" style="padding-left:5px;margin-left:5px"> <span style="text-transform: none">GPdI</span> <span style="font-size:20px">Manggar</span></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="oi oi-menu"></span> Menu
      </button>

      <div class="collapse navbar-collapse" id="ftco-nav">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item"><a href="{{route('user.beranda')}}" class="nav-link">Beranda</a></li>
          <li class="nav-item"><a href="{{route('user.tentangGereja')}}" class="nav-link">Tentang Gereja</a></li>
          <li class="nav-item"><a href="{{route('user.jadwalPelayanan')}}" class="nav-link">Jadwal Pelayanan</a></li>
          <li class="nav-item"><a href="{{route('user.jadwalIbadah')}}" class="nav-link">Jadwal Ibadah</a></li>
          <li class="nav-item"><a href="{{route('user.galeri')}}" class="nav-link">Galeri</a></li>
          {{-- <li class="nav-item"><a href="blog.html" class="nav-link">Blog</a></li>
          <li class="nav-item"><a href="contact.html" class="nav-link">Contact</a></li> --}}
        </ul>
      </div>
    </div>
  </nav>
<!-- END nav -->
<section id="home" class="video-hero js-fullheight" style="height: 760px; background-image: url({{URL::asset('user/images/bg_1.jpg')}});  background-size:cover; background-position: center center;background-attachment:fixed;" data-section="home">
    <div class="overlay js-fullheight"></div>
    {{-- <a class="player" data-property="{videoURL:'https://www.youtube.com/watch?v=5m--ptwd_iI',containment:'#home', showControls:false, autoPlay:true, loop:true, mute:false, startAt:0, opacity:1, quality:'default'}"></a> --}}
    <div class="container">
<div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center" data-scrollax-parent="true">
  <div class="col-md-10 ftco-animate text-center" data-scrollax=" properties: { translateY: '70%' }">
    <h1 class="mb-4" data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><strong>Walking With God</strong></h1>
    {{-- <p data-scrollax="properties: { translateY: '30%', opacity: 1.6 }"><a href="#" class="btn btn-primary btn-outline-white px-4 py-3">Save your spirit</a></p> --}}
  </div>
</div>
</div>
</section>

<section class="ftco-bible-study">
    <div class="container-wrap">
        <div class="col-md-12 wrap">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 d-md-flex">
                        <div class="one-forth ftco-animate">
                            <h3>Sudahkah Anda Membaca Firman?</h3>
                            <p>Renungkanlah Firman itu siang dan malam, supaya kehidupanmu berhasil dan beruntung.</p>
                        </div>
                        <div class="one-half d-md-flex align-items-md-center ftco-animate">
                            <div class="countdown-wrap">
                                <p class="countdown d-flex">
                                    <span id="days"></span>
                                    <span id="hours"></span>
                                    <span id="minutes"></span>
                                    <span id="seconds"></span>
                                </p>
                            </div>
                            <div class="button">
                                <p><a href="#" class="btn btn-primary p-3">Lihat Jadwal Bacaan</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>