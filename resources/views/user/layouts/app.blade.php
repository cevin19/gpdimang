<!DOCTYPE html>
<html lang="en">
  <head>
      @include('user.layouts.head')
  </head>
  <body @yield('body-style') @show>
    
	  @include('user.layouts.header')
    @section('content')
    @show
   

    @include('user.layouts.footer')
    
    <!-- loader -->
    <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>
    
</body>


{{-- <section class="ftco-section">
  <div class="container">
    <div class="row no-gutters justify-content-center mb-5 pb-5">
      <div class="col-md-7 text-center heading-section ftco-animate">
        <span class="subheading">Sermons</span>
        <h2 class="mb-4">Watch our sermons</h2>
        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in</p>
      </div>
    </div>
    <div class="row">
        <div class="col-md-4 ftco-animate">
            <div class="sermons">
                <a href="https://vimeo.com/45830194" class="img popup-vimeo mb-3 d-flex justify-content-center align-items-center" style="background-image: url(images/sermons-1.jpg);">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="icon-play"></span>
                    </div>
                </a>
                <div class="text">
                    <h3><a href="#">Be at Peace With One Another</a></h3>
                    <span class="position">Pastor. Joseph Meyer</span>
                </div>
            </div>
        </div>
        <div class="col-md-4 ftco-animate">
            <div class="sermons">
                <a href="https://vimeo.com/45830194" class="img popup-vimeo mb-3 d-flex justify-content-center align-items-center" style="background-image: url(images/sermons-2.jpg);">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="icon-play"></span>
                    </div>
                </a>
                <div class="text">
                    <h3><a href="#">Inspirational Message of God</a></h3>
                    <span class="position">Pastor. Joseph Meyer</span>
                </div>
            </div>
        </div>
        <div class="col-md-4 ftco-animate">
            <div class="sermons">
                <a href="https://vimeo.com/45830194" class="img popup-vimeo mb-3 d-flex justify-content-center align-items-center" style="background-image: url(images/sermons-3.jpg);">
                    <div class="icon d-flex justify-content-center align-items-center">
                        <span class="icon-play"></span>
                    </div>
                </a>
                <div class="text">
                    <h3><a href="#">Prayers, Presence, and Provision</a></h3>
                    <span class="position">Dave Zuleger</span>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col text-center">
            <p><a href="#" class="btn btn-primary btn-outline-primary p-3">Watch all sermons</a></p>
        </div>
    </div>
  </div>
</section>

<section class="ftco-section testimony-section bg-light">
  <div class="container">
    <div class="row justify-content-center mb-5 pb-5">
      <div class="col-md-7 text-center heading-section ftco-animate">
        <span class="subheading">Read, Get Inspired, and Share Your Story</span>
        <h2 class="mb-4">Testimonies</h2>
        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in</p>
      </div>
    </div>
    <div class="row ftco-animate">
      <div class="col-md-12">
        <div class="carousel-testimony owl-carousel ftco-owl">
          <div class="item text-center">
            <div class="testimony-wrap p-4 pb-5">
              <div class="user-img mb-4" style="background-image: url(images/person_1.jpg)">
                <span class="quote d-flex align-items-center justify-content-center">
                  <i class="icon-quote-left"></i>
                </span>
              </div>
              <div class="text">
                <p class="mb-5">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                <p class="name">Dennis Green</p>
                <span class="position">Member</span>
              </div>
            </div>
          </div>
          <div class="item text-center">
            <div class="testimony-wrap p-4 pb-5">
              <div class="user-img mb-4" style="background-image: url(images/person_2.jpg)">
                <span class="quote d-flex align-items-center justify-content-center">
                  <i class="icon-quote-left"></i>
                </span>
              </div>
              <div class="text">
                <p class="mb-5">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                <p class="name">Dennis Green</p>
                <span class="position">Volunteer</span>
              </div>
            </div>
          </div>
          <div class="item text-center">
            <div class="testimony-wrap p-4 pb-5">
              <div class="user-img mb-4" style="background-image: url(images/person_3.jpg)">
                <span class="quote d-flex align-items-center justify-content-center">
                  <i class="icon-quote-left"></i>
                </span>
              </div>
              <div class="text">
                <p class="mb-5">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                <p class="name">Dennis Green</p>
                <span class="position">Pastor</span>
              </div>
            </div>
          </div>
          <div class="item text-center">
            <div class="testimony-wrap p-4 pb-5">
              <div class="user-img mb-4" style="background-image: url(images/person_1.jpg)">
                <span class="quote d-flex align-items-center justify-content-center">
                  <i class="icon-quote-left"></i>
                </span>
              </div>
              <div class="text">
                <p class="mb-5">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                <p class="name">Dennis Green</p>
                <span class="position">Guest</span>
              </div>
            </div>
          </div>
          <div class="item text-center">
            <div class="testimony-wrap p-4 pb-5">
              <div class="user-img mb-4" style="background-image: url(images/person_1.jpg)">
                <span class="quote d-flex align-items-center justify-content-center">
                  <i class="icon-quote-left"></i>
                </span>
              </div>
              <div class="text">
                <p class="mb-5">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                <p class="name">Dennis Green</p>
                <span class="position">Pastor</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="ftco-section ftco-counter" id="section-counter">
  <div class="container">
    <div class="row justify-content-center mb-5 pb-5">
      <div class="col-md-7 text-center heading-section ftco-animate">
        <h2>Church Achievements</h2>
        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in</p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6 col-lg-4 d-flex justify-content-center counter-wrap ftco-animate">
        <div class="block-18 text-center">
          <div class="text">
            <strong class="number" data-number="20254">0</strong>
            <span>Churches around the world</span>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-4 d-flex justify-content-center counter-wrap ftco-animate">
        <div class="block-18 text-center">
          <div class="text">
            <strong class="number" data-number="4200000">0</strong>
            <span>Members around the globe</span>
          </div>
        </div>
      </div>
      <div class="col-md-6 col-lg-4 d-flex justify-content-center counter-wrap ftco-animate">
        <div class="block-18 text-center">
          <div class="text">
            <strong class="number" data-number="8600000">0</strong>
            <span>Save life &amp; Donations</span>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="ftco-section-2 bg-light">
    <div class="container-fluid">
        <div class="row no-gutters d-flex">
            <div class="col-md-6 img d-flex justify-content-end align-items-center" style="background-image: url(images/event.jpg);">
                <div class="col-md-7 heading-section text-sm-center text-md-right heading-section-white ftco-animate mr-md-5 mt-md-5">
            <h2>Our latest events</h2>
            <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in</p>
            <p><a href="#" class="btn btn-primary py-3 px-4">View Events</a></p>
          </div>
            </div>
            <div class="col-md-6">
                <div class="event-wrap">
                    <div class="event-entry d-flex ftco-animate">
                        <div class="meta mr-4">
                            <p>
                                <span>07</span>
                                <span>Aug 2018</span>
                            </p>
                        </div>
                        <div class="text">
                            <h3 class="mb-2"><a href="events.html">Saturday's Bible Reading</a></h3>
                            <p class="mb-4"><span>9:00am at 456 NC USA</span></p>
                            <a href="events.html" class="img" style="background-image: url(images/event-1.jpg);"></a>
                        </div>
                    </div>
                    <div class="event-entry d-flex ftco-animate">
                        <div class="meta mr-4">
                            <p>
                                <span>07</span>
                                <span>Aug 2018</span>
                            </p>
                        </div>
                        <div class="text">
                            <h3 class="mb-2"><a href="events.html">Wednesday Gospel Singing</a></h3>
                            <p class="mb-4"><span>9:00am at 456 NC USA</span></p>
                            <a href="events.html" class="img" style="background-image: url(images/event-2.jpg);"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="ftco-section">
  <div class="container">
    <div class="row justify-content-center mb-5 pb-5">
      <div class="col-md-7 text-center heading-section ftco-animate">
        <span class="subheading">Blog</span>
        <h2>Recent Blog</h2>
        <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in</p>
      </div>
    </div>
    <div class="row">
      <div class="col-md-4 ftco-animate">
        <div class="blog-entry">
          <a href="blog-single.html" class="block-20" style="background-image: url('images/image_1.jpg');">
          </a>
          <div class="text p-4 d-block">
            <div class="meta mb-3">
              <div><a href="#">July 12, 2018</a></div>
              <div><a href="#">Admin</a></div>
              <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
            </div>
            <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>
          </div>
        </div>
      </div>
      <div class="col-md-4 ftco-animate">
        <div class="blog-entry" data-aos-delay="100">
          <a href="blog-single.html" class="block-20" style="background-image: url('images/image_2.jpg');">
          </a>
          <div class="text p-4">
            <div class="meta mb-3">
              <div><a href="#">July 12, 2018</a></div>
              <div><a href="#">Admin</a></div>
              <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
            </div>
            <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>
          </div>
        </div>
      </div>
      <div class="col-md-4 ftco-animate">
        <div class="blog-entry" data-aos-delay="200">
          <a href="blog-single.html" class="block-20" style="background-image: url('images/image_3.jpg');">
          </a>
          <div class="text p-4">
            <div class="meta mb-3">
              <div><a href="#">July 12, 2018</a></div>
              <div><a href="#">Admin</a></div>
              <div><a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a></div>
            </div>
            <h3 class="heading"><a href="#">Even the all-powerful Pointing has no control about the blind texts</a></h3>
          </div>
        </div>
      </div>
    </div>
  </div>
</section> --}}


</html>