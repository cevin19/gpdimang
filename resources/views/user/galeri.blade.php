@extends('user.layouts.app')
@section('content')

<section class="ftco-section">
    <div class="container">
      <div class="row">
        @foreach($datas as $data)
        <div class="col-md-4 ftco-animate fadeInUp ftco-animated">
          <div class="blog-entry">
            <a href="{{$data->link}}" class="block-20" style="background-image:url({{$data->foto_depan}});">
            </a>
            <div class="text p-4 d-block">
              <h3 class="heading"><a href="{{$data->link}}">{{$data->nama}}</a></h3>
              
            </div>
          </div>
        </div>
        @endforeach
  
      </div>
    </div>
  </section>
@endsection