@extends('user.layouts.app')
@section('content')
<section class="ftco-section-2">
    <div class="container-fluid">
      <div class="section-2-blocks-wrapper d-flex row no-gutters">
        <div class="img col-md-6 ftco-animate" style="background-image: url({{URL::asset('user/images/about.jpg')}});">
          <a href="https://www.youtube.com/watch?v=cqglFfWjL_U" class="button popup-vimeo"><span class="ion-ios-play"></span></a>
        </div>
        <div class="text col-md-6 ftco-animate">
          <div class="text-inner ">
            
            <h3 style="margin-bottom:10px">{{$data->judul}}</h3>
            <div>
              <ul style="list-style:none;margin-left:-40px">
                <li>Khotbah : {{$data->pengkhotbah}}</li>
                <li>Ayat    : {{$data->ayat}}</li>
              </ul>
            </div>
            
            
            <p>{{$data->isi}}</p>

            {{-- <p></p> --}}
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="ftco-section">
    <div class="container">
      <div class="row justify-content-center mb-5 pb-5">
        <div class="col-md-6 text-center heading-section ftco-animate">
          <span class="subheading">Pelayanan Kami</span>
          <h2 class="mb-4">Jadilah Terang Bagi Dunia</h2>
          {{-- <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in</p> --}}
        </div>
      </div>
      <div class="row ">
        <div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate ">
          <div class="media block-6 services d-block text-center">
            <div class="d-flex justify-content-center "><div class="icon d-flex justify-content-center mb-3 "><span class="align-self-center flaticon-planet-earth"></span></div></div>
            <div class="media-body p-2 mt-3">
              {{-- <h3 class="heading">Peneriman Jemaat baru</h3> --}}
              {{-- <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p> --}}
            </div>
          </div>      
        </div>
        <div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate ">
          <div class="media block-6 services d-block text-center">
            <div class="d-flex justify-content-center "><div class="icon d-flex justify-content-center mb-3 "><span class="align-self-center flaticon-maternity"></span></div></div>
            <div class="media-body p-2 mt-3">
              {{-- <h3 class="heading">Bantuan Sosial</h3> --}}
              {{-- <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p> --}}
            </div>
          </div>      
        </div>
        <div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate ">
          <div class="media block-6 services d-block text-center">
            <div class="d-flex justify-content-center "><div class="icon d-flex justify-content-center mb-3 "><span class="align-self-center flaticon-pray"></span></div></div>
            <div class="media-body p-2 mt-3">
              {{-- <h3 class="heading">Bantuan Doa</h3> --}}
              {{-- <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p> --}}
            </div>
          </div>    
        </div>

        <div class="col-md-6 col-lg-3 d-flex align-self-stretch ftco-animate ">
          <div class="media block-6 services d-block text-center">
            <div class="d-flex justify-content-center "><div class="icon d-flex justify-content-center mb-3 "><span class="align-self-center flaticon-podcast"></span></div></div>
            <div class="media-body p-2 mt-3">
              {{-- <h3 class="heading">Kelompok-kelompok rohani</h3> --}}
              {{-- <p>Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.</p> --}}
            </div>
          </div>      
        </div>
      </div>
    </div>
  </section>

  <section class="ftco-section-parallax">
    <div class="parallax-img d-flex align-items-center">
      <div class="container">
        <div class="row d-flex justify-content-center">
          <div class="col-md-7 text-center heading-section heading-section-white ftco-animate">
            <h2>Warta Jemaat</h2>
            <p>Di warta jemaat ini, anda dapat melihat informasi seputar gereja seperti jadwal ibadah, jadwal pelayanan, informasi kegiatan tambahan, dan lain-lain.</p>
            <div class="row d-flex justify-content-center mt-5">
              <div class="col-md-6">
                <form action="#" class="subscribe-form">
                  <div class="form-group">
                   <a href="#warta-jemaat" class="btn btn-info btn-lg">Lihat Warta</a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

@endsection