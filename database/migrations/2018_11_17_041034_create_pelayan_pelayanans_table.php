<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePelayanPelayanansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pelayan_pelayanans', function (Blueprint $table) {
            $table->integer('pelayan_id')->unsigned()->index();
            $table->integer('jenis_pelayanan_id')->unsigned()->index();
            $table->foreign('pelayan_id')->references('id')->on('pelayans')->onDelete('cascade');
            $table->foreign('jenis_pelayanan_id')->references('id')->on('jenis_pelayanans')->onDelet('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pelayan_pelayanans');
    }
}
