<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJadwalPelayanansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwal_pelayanans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('waktu_ibadah');
            $table->string('whorship_leader');
            $table->string('singer_1');
            $table->string('singer_2')->nullable();
            $table->string('singer_3')->nullable();            
            $table->string('pemain_musik_1');
            $table->string('pemain_musik_2')->nullable();
            $table->string('pemain_musik_3')->nullable();
            $table->string('pemain_musik_4')->nullable();
            $table->string('multimedia');
            $table->string('kolektan_1');
            $table->string('kolektan_2');
            $table->string('penerima_tamu_1');
            $table->string('penerima_tamu_2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jadwal_pelayanans');
    }
}
